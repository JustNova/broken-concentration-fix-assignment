# BrokenConcentration

This is supposed to be a game of concentration, but it's broken. 

## Rules of the Game:

8 pairs of matching cards, each showing a Letter from A to H, are laid out in a 4x4 grid. 

Two players take turns choosing two of the cards to reveal, based on the row and column they are located in.

When a player chooses two cards that match (share the same letter), the two matching cards are removed from the grid. The player that made the match ngets a point, and gets to continue guessing. 

When a player chooses two cards that don't match, the cards are hidden again, and it becomes the opposing player's turn.

When all the cards have been matched and removed from the grid, the game is over and the player who made the most matches wins.